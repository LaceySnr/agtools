//======================================================================================================================
//	[A]tari [G]ame [T]ools / dml 2016
//======================================================================================================================
//	basic filesystem access
//----------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	system headers

#include <stdio.h>

// ---------------------------------------------------------------------------------------------------------------------
//	Our AGT headers

#include "common_cpp.h"

// basic memory interface
#include "ealloc.h"
#include "compress.h"

// ---------------------------------------------------------------------------------------------------------------------

size_t fsize(const char* file)
{
	size_t len = 0;
	FILE *fp = fopen(file, "r");
	if (fp)
	{
		fseek(fp, 0, SEEK_END);
		len = (size_t)ftell(fp);
		fclose(fp); fp = 0;
	}
	return len;
}

void fsave(char* name, char* data, int bytes)
{
	FILE *fp = fopen(name, "wb");
	if (fp)
	{
		fwrite(data, 1, bytes, fp);
		fclose(fp); fp = 0;
	}
}

// ---------------------------------------------------------------------------------------------------------------------
